require 'rubygems'
require 'domainatrix'

module StoragesHelper
  def strip_url(old_url)
    url = Domainatrix.parse(old_url)
    return url.domain
  end

  #new account for 7 day
  def new_for_week(storages)
    @week_day = 0
    @one_day  = 0
    current_time  = Time.now.strftime "%d.%m.%y"
    storages.each do |x|
      storages_time = x.updated_at.strftime "%d.%m.%y"
      new_time = current_time.to_i - storages_time.to_i
      new_time < 1   ?  @one_day  += 1 : @one_day = 0
      new_time <= 7  ?  @week_day += 1 : @week_day = 0
    end
    return @week_day
  end

  def new_for_day
    return @one_day
  end
end
