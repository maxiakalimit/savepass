class UsersController < ApplicationController
  def profile
    if current_user
      @storages = current_user.storages.all
    else
      #render json: "Вы должны авторизироваться на сайте http://savepass.xxivcreate.xyz/"
      render json: { error:true, message: "Вы должны авторизироваться на сайте http://savepass.xxivcreate.xyz/" }
    end

    if current_user && ( current_user.public_key == nil || current_user.exponenta == nil )
      n,exp,d = Cryptosyst.generate_keys( 1024 )
      current_user.update_attributes(:public_key => n, :exponenta =>  exp)
      File.open("#{Dir.pwd}/private_key/private_key-#{current_user.id}.pem", 'w'){ |file| file.write d }
    end
  end

  def index
    if current_user
      @users = User.all
      respond_to do |format|
        format.html
        format.json { render json: @users }
      end
    else
      #render json: "Вы должны авторизироваться на сайте http://savepass.xxivcreate.xyz/"
      render json: { error:true, message: "Вы должны авторизироваться на сайте http://savepass.xxivcreate.xyz/" }
    end
  end


  private
  def storage_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end
