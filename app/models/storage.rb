class Storage < ActiveRecord::Base
  belongs_to :user

  def as_json(options={})
    super(only: [:url, :name, :login, :password, :decoderlogin, :decoderpassword, :note, :logo, :favourite, :created_at, :updated_at])
  end

end
