//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
$(document).ready(function() {
  $(document).on('ready page:load', function() {
    //animation .animation_container
    (function($) {
      $.fn.floatBottomContainer = function() {
        $(this).animate({
          bottom: '-70px',
        }, 1000);
      }
    })(jQuery);

    //call function
    $('.animation_container').floatBottomContainer();

    $(".site-item").hover(function(event) {
      $(this).find(".site-settings").css({
        opacity: '1'
      });;
    }, function() {
      $(this).find(".site-settings").css({
        opacity: '.3',
        transition: '.5s'
      });;
    });

    //Function open form if click btn
    function openForm(classBtn, classForm, classMask) {
      $(classBtn).on('click', function(event) {
        event.preventDefault();
        $(classForm).css({
          display: 'block',
        });
        $(classMask).css({
          display: 'block',
        });
      });
    };

    //Function Hide form if click close btn
    function hideForm(classBtn, classForm, classMask) {
      $(classBtn).on('click', function(event) {
        event.preventDefault();
        $(classForm).css({
          display: 'none',
        });
        $(classMask).css({
          display: 'none',
        });
      });
    };

    openForm(".btn-add", ".form-add", ".mask");
    openForm(".btn-add-content", ".form-add", ".mask");
    hideForm(".mask", ".form-add", ".mask");
    hideForm(".form-add-close", ".form-add", ".mask");
    $(function() {
      var c = true;
      $(".profile-inform").on("click", function(event) {
        if (c == true) {
          $(".menuList").css("display", "block");
          c = false;
        } else {
          $(".menuList").css("display", "none");
          c = true;
        }
      });

    });

  });

  var $searchTrigger = $('[data="search-trigger"]'),
    $searchInput = $('[data="search-input"]'),
    $searchClear = $('[data="search-clear"]');

  $searchTrigger.click(function() {

    var $this = $('[data="search-trigger"]');
    $this.addClass('active');
    $searchInput.focus();

  });

  $searchInput.blur(function() {
    if ($searchInput.val().length > 0) {
      return false;
    } else {
      $searchTrigger.removeClass('active');
    }
  });

  $searchClear.click(function() {
    $searchInput.val('');
  });

  $searchInput.focus(function() {
    $searchTrigger.addClass('active');
  });

  function gridOrlist(btn, btn2, container, container2) {
    $(btn).click(function(event) {
      $(btn2).css({
        opacity: '0.5',
      });
      $(this).css({
        opacity: "1",
      });
      $(container).css({
        display: "block",
      });
      $(container2).css({
        display: "none",
      });
    });
  };
  gridOrlist(".grid-or-list .list", ".grid-or-list .grid", ".site-content-list", ".site-content-grid");
  gridOrlist(".grid-or-list .grid", ".grid-or-list .list", ".site-content-grid", ".site-content-list");


  //AJAX
  //Del
  $(".btn-settings .del").click(function() {
    var current_item = $(this).parents(".cur_item")[0];
    $.ajax({
      url: '/storages/' + $(current_item).attr('data-item_id'),
      type: 'POST',
      data: {
        _method: "delete"
      },
      error: function() {
        console.log("500");
      },
      success: function(result) {
        $(current_item).animate({
          opacity: '0',
        }, 100, function() {
          $(current_item).css({
            display: 'none',
          });
        });
        console.log(result);
      }
    });
  });

  //Favourite
  $(".btn-settings .star").click(function() {
    var current_item = $(this).parents(".cur_item")[0];
    if ($(current_item).attr('data-item_favourite') == "false") {
      $.ajax({
        url: '/storages/' + $(current_item).attr('data-item_id'),
        type: 'POST',
        data: {
          _method: "true"
        },
        error: function() {
          console.log("500");
        },
        success: function(result) {}
      });
    }
  });

  function notification() {
    $(".notification").animate({
      bottom: "0",
      opacity: "1",
    }, 700);
    setTimeout(function() {
      $(".notification").animate({
        bottom: "-60px",
        opacity: "0",
      }, 1000);
    }, 5000);
  }

  $(".simpleForm").on("ajax:success", function(e, data, status, xhr) {
    $(".form-add").fadeOut();
    $(".mask").fadeOut();
    $(".inputForm:not(.phone_form)").val("");
    $(".profile").html(data);
    console.log(status);
    notification();
  }).on("ajax:error", function(e, xhr, status, error) {

  });

  //COPY TEXT
  (function() {
    function copy(text) {
      window.prompt('Скопировать в буфер: Ctrl+C, Enter', text);
    };

    $('.copy-login').on('click', function() {
      var text_login = $($(this).siblings('.hover-login')).text();
      copy(text_login);
    });
    $('.copy-pass').on('click', function() {
      var text_pass = $($(this).siblings('.hover-pass')).text();
      copy(text_pass);
    });
    //$('.prompt').css('animation', 'blinker 1.5s ease-in-out infinite');
  }.call(this));
});
