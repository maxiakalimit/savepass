# m     - сообщение
###############################################
##  PRIVATE
##
#   d     - приватная экспонента
#   (d,n) - приватный ключ
#   ϕ(n)  - Функция Эйлера
###############################################
##  PUBLIC
##
#   c     - шифрованный текст
#   n     - публичный модуль
#   exp   - публичная экспонента
##  (exp,n) - публичный ключ''
###############################################
##
#

class Integer

   #Выполняет нахождение простого числа эффективным способом
   def prime?
     n = self.abs
     if n <=1 || n % 2 == 0
       return false
     elsif n == 2
       return true
     end
     d = n-1
     d >>= 1 while d & 1 == 0
     20.times do
       a = rand(n-2) + 1
       t = d
       y = Integer.modpow( a, t, n )
       while t != n-1 && y != 1 && y != n-1
         y = (y * y) % n
         t <<= 1
       end
       return false if y != n-1 && t & 1 == 0
     end
     return true
   end

   #Выполняет модульное возведение в степень эффективным способом
   #https://docs.omniref.com/ruby/gems/rsa/0.1.2/symbols/RSA::Math/modpow/singleton
   def self.modpow( base, exp, mod )
     result = 1
     base = base % mod
     while exp > 0
       result = ( result * base) % mod if exp & 1 == 1
       base = ( base * base ) % mod
       exp >>= 1
     end
     return result
   end
end


class Cryptosyst

  #Числа Ферма
  #https://ru.wikipedia.org/wiki/%D0%A7%D0%B8%D1%81%D0%BB%D0%B0_%D0%A4%D0%B5%D1%80%D0%BC%D0%B0
  Exp = 0x10001   #65537

  # Генерирует публичный модуль - n, публичную экспоненту - exp и приватный ключ - d.
  def self.generate_keys( bits )
    n, exp, d = 0
    p = random_prime( bits )
    q = random_prime( bits )
    n = p * q
    d = get_d( p, q, Exp )
    [n, Exp, d]
  end

  # Шифрует текст, используя публичный модуль n
  def self.encrypt( m, n )
    m = s_to_n( m )
    return Integer.modpow( m, Exp, n )
  end

  # Расшифровывает шифр, используя приватную экспоненту d
  def self.decrypt( c, n, d )
    #c = hex_to_n(c)
    m = Integer.modpow( c, d, n )
    return n_to_s( m )
  end


  private

  # Число в строку
  def self.n_to_s( n )
    str = ""
    while( n > 0 )
      str = ( n & 0xFF ).chr + str
      n >>= 8
    end
    return str
  end

  # Строка в число
  def self.s_to_n( str )
    n = 0
    str.each_byte {|b| n = n * 256 + b }
    return n
  end

  #Number to HEX
  #def self.to_hex ( n )
  #  return "%x" % n
  #end

  #HEX TO Number
  #def self.to_num ( hex )
  #  return hex.to_i(16)
  #end

  # Генерируется случайное простое число
  def self.random_prime( bits )
    begin
      n = random_number( bits )
      return n if n.prime?
    end while true
  end

  # Добавляется 1 в начало + 1 в конец, чтобы получить желаемую длину bits.
  # Например bits = 1024
  def self.random_number( bits )
    m = (1..bits-2).map{ rand() > 0.5 ? '1' : '0' }.join
    s = "1" + m + "1"
    return s.to_i( 2 )
  end

  # Функция Эйлера  φ(p,q)
  # https://ru.wikipedia.org/wiki/%D0%A4%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D1%8F_%D0%AD%D0%B9%D0%BB%D0%B5%D1%80%D0%B0
  def self.phi( a, b )
    return (a - 1) * (b - 1)
  end

  # Расширенный алгоритм Евклида
  def self.extended_gcd( a, b )
    return [0,1] if a % b == 0
    x, y = extended_gcd( b, a % b )
    return [y, x - y * (a / b)]
  end

  # Вычисляем d;   e * d  = 1 (mod φ(p,q)),
  def self.get_d(p, q, exp)
    t = phi( p, q )
    x, y = extended_gcd( exp, t )
    x += t if x < 0
    return x
  end
end
