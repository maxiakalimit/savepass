Rails.application.routes.draw do
  resources :storages

  devise_for :users
  get 'users/profile'
  get 'users/settings'

  authenticated :user do
    root :to => "users#profile"
  end

  resources :users do
    member do
      get :storage
    end
  end

  unauthenticated :user do
    devise_scope :user do
      get "/" => "main#index"
    end
  end
end
